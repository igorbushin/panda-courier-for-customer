//
//  RateViewController.m
//  Panda courier for customer
//
//  Created by ingwine on 8/26/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "RateViewController.h"
#import "Forward.h"

@interface RateViewController ()

@end

@implementation RateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [GuiHelper renameBackButtonOnView:self];
    _rateView.starCount = 5;
    _rateView.starSize = 50;
    _rateView.rating = 2.5;
    _rateView.canRate = YES;
    _rateView.starNormalColor = [UIColor whiteColor];
    _rateView.starFillColor = [UIColor yellowColor];
    _rateView.starBorderColor = [UIColor blackColor];
    _rateView.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)returnToMainMenuTapped:(UIButton *)sender {
    //TODO: send rate data to server
    [self.navigationController popToRootViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - RateViewDelegate Methods

-(void)rateView:(RateView*)rateView didUpdateRating:(float)rating
{
    
}
@end
