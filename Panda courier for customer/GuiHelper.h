//
//  GuiHelper.h
//  Panda courier for customer
//
//  Created by ingwine on 8/26/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface GuiHelper : NSObject

+(void)renameBackButtonOnView:(UIViewController*)viewController;
+(void)setTrueStyleForButton:(UIButton*)button;

@end
