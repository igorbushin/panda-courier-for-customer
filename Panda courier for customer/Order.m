//
//  Order.m
//  Test client
//
//  Created by ingwine on 8/22/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "Order.h"
#import "WebHelper.h"

@implementation Order

+(Order *)fakeOrder{
    Order *order = [[Order alloc] init];
    order.id = 1;
    order.userId =765589310338924544L;
    order.courierId = 719454948757544960L;
    order.price = 4;
    order.goods = @"5";
    order.targetLocation = @"6";
    order.timestamp = 7;
    order.isActiveOrder = true;
    return order;
}

-(Order *)init{
    if(!(self = [super init])) return nil;
    _courierId = -1;
    _goods = @"";
    _targetLocation = @"";
    return self;
}

-(NSString*)description{
    //NSMutableDictionary *dictionary = [WebHelper makeDictionaryFromObject:self];
    NSMutableString *result = [NSMutableString stringWithFormat:@""];
    /*for (NSString *key in dictionary){
        [result appendFormat:@"%@=%@\n", key, [dictionary valueForKey:key]];
    }*/
    [result appendFormat:@"Товар: %@\n", _goods];
    [result appendFormat:@"Цена: %lld\n", _price];
    [result appendFormat:@"Адрес: %@\n", _targetLocation];
    [result appendFormat:@"Timestamp: %lld\n", _timestamp];
    [result appendFormat:@"Id: %lld\n", _id];
    return result;
}



@end
