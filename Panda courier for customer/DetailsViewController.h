//
//  DetailsViewController.h
//  Panda courier for customer
//
//  Created by ingwine on 8/26/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface DetailsViewController : UIViewController
- (IBAction)callCourierTapped:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet UITextView *textView;
- (IBAction)returnToMenuTapped:(id)sender;
@property long long orderId;
@property Order *curOrder;
@end
