//
//  Model.m
//  Panda Courier Client ObjC Model
//
//  Created by ingwine on 8/24/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "Model.h"
#import "WebHelper.h"
#import "Forward.h"

@implementation Model

+(Model*)sharedInstance{
    
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    
    return sharedInstance;
}

-(Model*)init{
    if(!(self = [super init])) return nil;
    _user = [[User alloc] init];
    _orders = [[NSMutableArray alloc] init];
    _couriers = [[NSMutableDictionary alloc] init];
    _timerForUpdates = [NSTimer scheduledTimerWithTimeInterval: 120 target: self
                                                      selector:@selector(pullChangesFromServerOnTick:)
                                                      userInfo: nil repeats:YES];
    return self;
}

-(void)tryLogin{
    Digits *digits = [Digits sharedInstance];
    [digits authenticateWithCompletion:^(DGTSession *session, NSError *error) {
        if ([session userID]) {
            _user.isLogged = TRUE;
            Digits *digits = [Digits sharedInstance];
            DGTOAuthSigning *oauthSigning = [[DGTOAuthSigning alloc] initWithAuthConfig:digits.authConfig authSession:digits.session];
            NSDictionary *authHeaders = [oauthSigning OAuthEchoHeadersToVerifyCredentials];
            NSLog(@"%@", authHeaders);
            _user.credentials = authHeaders;
            _user.digitsId = [session userID];
            _user.phoneNumber = [session phoneNumber];
            
        } else if (error) {
            NSLog(@"Authentication error: %@", error.localizedDescription);
        }
    }];
}

-(void)tryHardcodeLogin{
    
    //crap start
    [_user setPhoneNumber:@"+79610826559"];
    [_user setDigitsId:@"765589310338924544"];
    [_user setCredentials:@{@"X-Auth-Service-Provider" : @"https://api.digits.com/1.1/sdk/account.json",
                            @"X-Verify-Credentials-Authorization" : @"OAuth oauth_timestamp=\"1472211180\",oauth_version=\"1.0\",oauth_consumer_key=\"Wzgx7Qnk5fQXoI1Tj3MdLVbxL\",oauth_signature=\"qDK%2BITmm0yl36sos9xMJZ1mdP9w%3D\",oauth_token=\"765589310338924544-dlXzbiaAt73CqeQVg8HB96VbMfWfvOT\",oauth_nonce=\"5135F2DD-4C9A-44EA-A336-B10591F31B53\",oauth_signature_method=\"HMAC-SHA1\""}];

    //TODO: log on server
    [_user setIsLogged:TRUE];
    //crap end
}

-(void)logout{
    [[Digits sharedInstance] logOut];
    [_user setIsLogged:FALSE];
    _user.credentials = @{};
}

-(void)pushOrder:(Order*)order{
    [WebHelper makeMethod:@"POST" withURLpath:@"orders" requestEntity:order additionalHeaders:_user.credentials
    andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {NSLog(@"Done");}];
}

-(void)pullOrders{
    [WebHelper makeMethod:@"GET" withURLpath:@"user/curOrders" requestEntity:nil additionalHeaders:_user.credentials
    andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error){NSLog(@"%@", error.description);return;}
        NSError *parseError;
        NSArray *parsedResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        if(parseError){NSLog(@"%@", parseError.description);return;}
        NSMutableArray *orders = [[NSMutableArray alloc] init];
        for (NSDictionary *dictinary in parsedResponse){
            Order *object = [WebHelper makeObjectWithType:[Order class] fromDictionary:dictinary];
            [orders addObject:object];
        }
        Model *model = [Model sharedInstance];
        [model setOrdersWithKeyValueStyle:orders];
        //[model setOrders:orders];
        NSLog(@"%@", model.orders);
  }];
}

-(void)setOrdersWithKeyValueStyle:(NSMutableArray *)orders{
    [self willChangeValueForKey:@"orders"];
    _orders = orders;
    [self didChangeValueForKey:@"orders"];
}

-(void)updateLocationOfCourierByOrderId:(NSInteger)orderId{
    NSString *urlAsString = [NSString stringWithFormat:@"user/courierCoordinates/%ld", (long)orderId];
    [WebHelper makeMethod:@"GET" withURLpath:urlAsString requestEntity:nil additionalHeaders:_user.credentials andCompletionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSError *parseError;
        NSDictionary *parsedResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
        if(parseError){NSLog(@"%@", parseError.description);return;}
        Model *model = [Model sharedInstance];
        [model.couriers setValue:parsedResponse forKey:[NSString stringWithFormat:@"%ld", (long)orderId]];
    }];
}

-(Order *)getOrderById:(long long)orderId{
    for (Order *order in _orders)
    {
        if(order.id == orderId)
            return order;
    }
    return nil;
}

-(void)pullChangesFromServerOnTick:(NSTimer *)timer{
    if([_orders count] < 10)
    /*{
        [self willChangeValueForKey:@"orders"];
        [_orders addObject:[Order fakeOrder]];
        for (Order *order in _orders)
            order.price++;
        [self didChangeValueForKey:@"orders"];
    }
    */
    if(!_user.isLogged) return;
    [self pullOrders];
}

@end
