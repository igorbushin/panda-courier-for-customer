//
//  GuiHelper.m
//  Panda courier for customer
//
//  Created by ingwine on 8/26/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "GuiHelper.h"

@implementation GuiHelper

+(void)renameBackButtonOnView:(UIViewController*)viewController{
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"Назад";
    viewController.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
}

+(void)setTrueStyleForButton:(UIButton*)button insideViewController:(UIViewController*)viewController{
    //button.leftAnchor = viewController.view.leftAnchor;
}

@end
