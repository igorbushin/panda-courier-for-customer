//
//  OrdersViewController.m
//  Panda courier for customer
//
//  Created by ingwine on 8/25/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "OrdersViewController.h"
#import "DetailsViewController.h"
#import "Forward.h"

@interface OrdersViewController ()

@end

@implementation OrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [GuiHelper renameBackButtonOnView:self];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated{
    Model *model = [Model sharedInstance];
    [model addObserver:self forKeyPath:@"orders" options:0 context:nil];// add kvo for orders
    [model pullOrders];
}

- (void)viewWillDisappear:(BOOL)animated{
    Model *model = [Model sharedInstance];
    [model removeObserver:self forKeyPath:@"orders"];// remove kvo for orders
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    //TODO: check is changed orders in model
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    Model *model = [Model sharedInstance];
    return [model.orders count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Model *model = [Model sharedInstance];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.textLabel.text = [[model.orders objectAtIndex:indexPath.row] goods];
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    id newViewController = [segue destinationViewController];
    if([newViewController isMemberOfClass:[DetailsViewController class]])
    {
        Model *model = [Model sharedInstance];
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        [newViewController setOrderId:[[model.orders objectAtIndex:[indexPath row]] id]];
    }
}


@end
