//
//  Helper.h
//  Test client
//
//  Created by ingwine on 8/24/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <Foundation/Foundation.h>

#define serverAddress @"http://192.168.0.100:8080/api/v1"

@interface WebHelper : NSObject

+(void)makeMethod:(NSString*)method withURLpath:(NSString*)path requestEntity:(id)object
additionalHeaders:(NSDictionary*) additionalHeaders
andCompletionHandler: (void (^)(NSData*, NSURLResponse*, NSError* ))completionHandler;

+(NSMutableDictionary *)makeDictionaryFromObject:(id)object;
+(id)makeObjectWithType:(Class)class fromDictionary:(NSDictionary*)dictionary;

@end
