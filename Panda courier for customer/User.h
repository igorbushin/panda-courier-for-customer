//
//  User.h
//  Panda Courier Client ObjC Model
//
//  Created by ingwine on 8/24/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
@property BOOL isLogged;
@property NSString *phoneNumber;
@property NSString *digitsId;
@property NSDictionary *credentials;
@end
