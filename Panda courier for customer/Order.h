//
//  Order.h
//  Test client
//
//  Created by ingwine on 8/22/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Order : NSObject
@property long long id;
@property NSString *goods;
@property long long price;
@property long long userId;
@property long long courierId;
@property NSString *targetLocation;
@property long long timestamp;
@property bool isActiveOrder;
-(NSString*)description;
+(Order *)fakeOrder;
@end