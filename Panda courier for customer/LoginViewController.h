//
//  LoginViewController.h
//  Panda courier for customer
//
//  Created by ingwine on 8/25/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *orderButton;
@property (strong, nonatomic) IBOutlet UIButton *myOrdersButton;
@property (strong, nonatomic) IBOutlet UIButton *logInOutButton;
-(IBAction)logInOutTapped:(UIButton *)sender;


@end
