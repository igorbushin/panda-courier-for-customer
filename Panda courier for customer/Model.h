//
//  Model.h
//  Panda Courier Client ObjC Model
//
//  Created by ingwine on 8/24/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

@import Foundation;
@import CoreLocation;
#import "Order.h"
#import "User.h"

@interface Model : NSObject //must init first & die last
@property NSTimer *timerForUpdates;
@property User *user;
@property NSMutableArray *orders;
@property NSMutableDictionary *couriers;

+ (Model*)sharedInstance;
-(Model*)init;
-(void)tryLogin;
-(void)logout;
-(void)setOrdersWithKeyValueStyle:(NSMutableArray*)orders;
-(void)pushOrder:(Order*)order;
-(void)pullOrders;
-(void)updateLocationOfCourierByOrderId:(NSInteger)id;
-(void)pullChangesFromServerOnTick:(NSTimer *)timer;
-(Order *)getOrderById:(long long)orderId;

@end
