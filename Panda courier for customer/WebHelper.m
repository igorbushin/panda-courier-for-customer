//
//  Helper.m
//  Test client
//
//  Created by ingwine on 8/24/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "WebHelper.h"
#import <objc/runtime.h>

@implementation WebHelper

+(NSMutableDictionary *)makeDictionaryFromObject:(id)object{
    
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    unsigned propertiesCount;
    objc_property_t *properties = class_copyPropertyList([object class], &propertiesCount);
    for (int i = 0; i < propertiesCount; i++) {
        NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
        [dictionary setObject:[object valueForKey:key] forKey:key];
    }
    free(properties);
    return dictionary;
}

+(id)makeObjectWithType:(Class)class fromDictionary:(NSDictionary*)dictionary{
    
    id object = [[class alloc] init];
    [object setValuesForKeysWithDictionary:dictionary];
    /*
    for (NSString *key in dictionary){
        [object setValue:[dictionary valueForKey:key] forKey:key];
    }
     */
    return object;
}

+(void)makeMethod:(NSString*)method withURLpath:(NSString*)path requestEntity:(id)object

    additionalHeaders:(NSDictionary*) additionalHeaders
    andCompletionHandler: (void (^)(NSData*, NSURLResponse*, NSError* ))completionHandler{
    
    NSString *urlAsString = [NSString stringWithFormat:@"%@/%@", serverAddress, path];
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    if(additionalHeaders){
        for (NSString *key in additionalHeaders){
            [request setValue:[additionalHeaders valueForKey:key] forHTTPHeaderField:key];
        }
    }
    if([method isEqualToString:@"POST"]){
        NSMutableDictionary *JSONobject = [WebHelper makeDictionaryFromObject:object];
        NSData *data = [NSJSONSerialization dataWithJSONObject:JSONobject options:0 error:nil];
        [request setHTTPBody:data];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%ld", (long)[data length]] forHTTPHeaderField:@"Content-Length"];
    }
    NSURLSession *session = [NSURLSession sharedSession];
    [session dataTaskWithRequest:request];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:completionHandler];
    [task resume];
}

@end
