//
//  ChooseGoodsViewController.m
//  Panda courier for customer
//
//  Created by ingwine on 8/26/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "ChooseGoodsViewController.h"
#import "AddressViewController.h"
#import "Forward.h"

@interface ChooseGoodsViewController ()

@end

@implementation ChooseGoodsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [GuiHelper renameBackButtonOnView:self];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    id newViewController = [segue destinationViewController];
    if([newViewController isMemberOfClass:[AddressViewController class]] &&
       [sender isMemberOfClass:[UIButton class]])
    {
        Order *order = [[Order alloc] init];
        UIButton *button = sender;
        order.goods = [[button titleLabel] text];
        [newViewController setOrder:order];
    }
    
}


@end
