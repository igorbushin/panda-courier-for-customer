//
//  AddressViewController.m
//  Panda courier for customer
//
//  Created by ingwine on 8/26/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "AddressViewController.h"
#import "DetailsViewController.h"
#import "Forward.h"

@interface AddressViewController ()

@end

@implementation AddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [GuiHelper renameBackButtonOnView:self];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.86
                                                            longitude:151.20
                                                                 zoom:6];
    GMSMapView *mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    self.view = mapView_;
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
    marker.title = @"Sydney";
    marker.snippet = @"Australia";
    marker.map = mapView_;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    id newViewController = [segue destinationViewController];
    if([newViewController isMemberOfClass:[DetailsViewController class]])
    {
        Model *model = [Model sharedInstance];
        _order.id = -1;
        _order.timestamp = random();
        _order.price = random();
        _order.isActiveOrder = TRUE;
        [newViewController setCurOrder:_order];
        [newViewController setOrderId:-1];//<--wtf!
        [model pushOrder:_order];
    }
}

@end
