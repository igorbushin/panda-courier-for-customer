//
//  RateViewController.h
//  Panda courier for customer
//
//  Created by ingwine on 8/26/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RateView.h"

@interface RateViewController : UIViewController
<RateViewDelegate>
- (IBAction)returnToMainMenuTapped:(UIButton *)sender;
@property (strong, nonatomic) IBOutlet RateView *rateView;
@end
