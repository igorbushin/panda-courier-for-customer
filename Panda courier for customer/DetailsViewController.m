//
//  DetailsViewController.m
//  Panda courier for customer
//
//  Created by ingwine on 8/26/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "DetailsViewController.h"
#import "Forward.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [GuiHelper renameBackButtonOnView:self];
}

- (void)viewWillAppear:(BOOL)animated{
    Model *model = [Model sharedInstance];
    if(_orderId == -1){
        [_textView setText:[_curOrder description]];
    }
    else{
        Order *order = [model getOrderById:_orderId];
        if(!order) return;//TODO: alert "no order" + go back
        [_textView setText:[order description]];
    }
    [model addObserver:self forKeyPath:@"orders" options:0 context:nil];// add kvo for orders
}

- (void)viewWillDisappear:(BOOL)animated{
    Model *model = [Model sharedInstance];
    [model removeObserver:self forKeyPath:@"orders"];// remove kvo for orders
    //TODO:...
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    //TODO: check is changed orders in model
    Model *model = [Model sharedInstance];
    Order *order = [model getOrderById:_orderId];
    if(!order) return;//TODO: alert "no order" + go back
    [_textView setText:[order description]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)returnToMenuTapped:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)callCourierTapped:(UIButton *)sender {
    //TODO: replace face number with courier's number
    NSString *phoneNumber = [@"tel://" stringByAppendingString:@"+79610826559"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

@end
