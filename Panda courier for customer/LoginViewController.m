//
//  LoginViewController.m
//  Panda courier for customer
//
//  Created by ingwine on 8/25/16.
//  Copyright © 2016 ingwine. All rights reserved.
//

#import "LoginViewController.h"
#import "Forward.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //TODO: is kvo here right?
    Model *model = [Model sharedInstance];
    [model addObserver:self forKeyPath:@"user.isLogged" options:0 context:nil];
    [model tryLogin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)logInOutTapped:(UIButton *)sender {
    Model *model = [Model sharedInstance];
    BOOL isUserLogged = model.user.isLogged;
    isUserLogged ? [model logout] : [model tryLogin];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    
    //TODO: add check is changed isLogged property in model
    Model *model = [Model sharedInstance];
    BOOL isUserLogged = model.user.isLogged;
    [_orderButton setEnabled:isUserLogged];
    [_myOrdersButton setEnabled:isUserLogged];
    [_logInOutButton setTitle:(isUserLogged ? @"Выйти" : @"Войти") forState:UIControlStateNormal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
